// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "tabraq" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand(
		'extension.TABraq',
		() => {
			let editor = vscode.window.activeTextEditor;

			if (editor) {
				let document = editor.document;
				let selection = editor.selection;
				
				let str = document.getText(selection); // Get the selection text
				var lines = str.split('\n');
				var pyStr = ''
				const leftbraq ="{"
				const rightbraq ="}"
				for(var i = 0; i < lines.length; i++) 
				{
					var thereIsLeftbraq = lines[i].includes(leftbraq)
					var thereIsRightbraq = lines[i].includes(rightbraq)
					if( !thereIsLeftbraq && !thereIsRightbraq )
					{
						pyStr = concatStr(pyStr, lines[i].trimRight(), i)
					}
					else
					{
						var toSearch = ''
						if(thereIsLeftbraq)
						{
							toSearch = leftbraq
						}
						else
						{
							toSearch = rightbraq
						}

						/* 
						 getting the string before the braq in case of the braq is in the line of the statment.
						 for example: 'while(True): {' , 'if(True){' etc.
						*/
						var beforeBraqStr = lines[i].substr(0, lines[i].search(toSearch)) 
						if (!isNullOrWhiteSpace(beforeBraqStr)) 
						{
							pyStr = concatStr(pyStr, beforeBraqStr.trimRight(), i)
						
						}
						/*
						 In Python, after statment comes ':'. for exampple: 'while(True):'
						 In this statment we add those ':' if we deleted the '{'
						 For example: 'while(True){' is now 'while(True):' 
						*/
						if ( thereIsLeftbraq ) 
						{
							pyStr = pyStr.concat(':')
						}
					}
				}								
				
				editor.edit(editBuilder => {
					editBuilder.replace(selection, pyStr);
				});
			}
	});

	context.subscriptions.push(disposable);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate
}



function isNullOrWhiteSpace(str) 
{
  return (!str || str.length === 0 || /^\s*$/.test(str))
}
function concatStr(source, toAdd, i)
{
    if(i != 0) // if it's the first line, we don't add "\n" at the beginning
    {
        source = source.concat("\n")
    }
    source = source.concat(toAdd)

    return source
}
